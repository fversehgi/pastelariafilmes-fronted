#Template Frontend com Grunt, Bower e Initializr

Este é um template frontend que já contém:

* Bower com pacotes básicos já listados (Jquery, FontAwesome, Bootstrap3Grid, Normalize)
* Grunt com compile de SASS, minify de CSS e JS e LiveReload
* Initializr com <head> completo e costumizavel
* Padrões de boas práticas da Atua Agência

## Instalação

Clone o repositório

```
git clone git@bitbucket.org:atuaagencia/grunt-frontend-template.git
```

Renomeie o diretório
```
mv grunt-frontend-template/ nome-do-projeto
```

Acesse a pasta e delete o GIT
```
cd nome-do-projeto
rm -rf .git
```

Inicie um novo repositório
```
git init
...
```

Modifique os títulos dos project files
```
bower.json -> Mudar o "name": "appName"
package.json -> Mudar o "name": "AppName"
```

Caso seja a primeira vez que roda Grunt na máquina
```
npm install -g grunt-cli
```

Instale as dependencias
```
bower install
npm install
```
Copie as fonts do FontAwesome para suas pastas
```
grunt copy
```

Ligue o servidor
```
grunt server
```
E **seja feliz :)**

#Observações

Caso queira deletar ulguma pasta e dê o erro de que ela é muito longa, utilizar:
```
npm install -g rimraf
rimraf <dir>
```