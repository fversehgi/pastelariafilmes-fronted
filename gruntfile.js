module.exports = function(grunt){
    grunt.initConfig({
        pkg:grunt.file.readJSON('package.json'),
 
        // Faz o Watch dos assets de SASS, SCSS, CSS e JS
        watch:{
            options:{livereload:true},
            files:['app/styles/*.sass','app/styles/*.html','app/styles/*.css'],
            tasks:[],
            css:{
                files: ['app/styles/*.sass'],
                tasks: ['sass'],
                options: {
                    //spawn: false
                }
            },
            js:{
                files: ['app/js/*.js'],
                tasks: ['uglify'],
                options: {
                    spawn: false
                }
            },
            minicss:{
                files: ['app/components/*.css'],
                tasks: ['cssmin'],
                options: {
                    spawn: false
                }
            }
        },

        // Compila o SASS
        sass:{
            dist:{
                options:{
                    style: 'compressed'
                },
                files:{
                    '../style.css': 'app/styles/main.sass'
                }
            }
        },

        // Minifica os javascripts em 1 arquivo, o app/src/main.min.js
        // Novos arquivos JS instalados pelo bower precisam ser adicionados aqui
        uglify:{
            build:{
                files:{
                    '../js/main.min.js': [
                        'app/components/jquery/dist/jquery.js', 
                        'app/components/owl-carousel2/dist/owl.carousel.js', 
                        'app/js/main.js' // main.js vem por último
                    ]                    
                }
            }
        },

        // Minifica os CSS em 1 arquivo, o app/src/style.min.js
        // Novos arquivos CSS instalados pelo bower precisam ser adicionados aqui
        cssmin:{
            options:{
                shorthandCompacting: false,
                roudingPrecision: -1
            },
            target:{
                files:{
                    '../plugins.min.css': [
                        'app/components/font-awesome/css/font-awesome.min.css',
                        'app/components/normalize-css/normalize.css',
                        'app/components/bootstrap-grid/grid.min.css',
                        'app/components/owl-carousel2/dist/assets/owl.theme.default.min.css',
                        'app/components/owl-carousel2/dist/assets/owl.carousel.min.css'
                        // adicionar novos arquivos aqui
                        //'app/styles/compiled/main.css' // main.css vem por último
                    ]
                }
            }
        },

        // Copia as fontes instaladas pelo bower para a pasta app/src/fonts
        // Novas fontes devem ser adicionadas aqui
        // Para executar, digita grunt copy no console
        copy:{
            main:{
                expand: true, 
                cwd: 'app/components/font-awesome/fonts/', 
                src: ['**'], 
                dest: 'app/src/fonts/', 
                filter: 'isFile'
            }
        },

        // Livereload de tudo que estiver na pasta app
        express:{
            all:{
                options:{
                    port:3000,
                    hostname:'localhost',
                    bases:['./app'],
                    livereload:true 
                }
            }
        }
    });
 
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-express');    
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.registerTask('server',['express','uglify','watch']);
 
    };