var scrolltop_buffer = 0;

$(document).ready(function() {

	// Open menu
	$('.menu-trigger').click(function(){
		$('body').toggleClass('menu-active');
	});

	// Close menu
	$('.fader').click(function(){
		$('body').toggleClass('menu-active');
	});

	// Open submenu
	$('nav .go-down a').click(function(){
		$(this).addClass('selected');
		$(this).parent().children('ul').addClass('active');
	});

	// Close submenu
	$('nav .go-up').click(function(e){
		$('nav .go-down a').removeClass('selected');
		e.preventDefault();
		$(this).parent().parent('ul').removeClass('active');
	});

	// Open Cardapio Tooltip
	$('nav ul li ul a').mouseenter(function(event) {
		$('.cardapio-tooltip').addClass('active');
	});

	$('nav ul li ul a').mouseleave(function(event) {
		$('.cardapio-tooltip').removeClass('active');
	});

	// Home carousel
	$('.owl-home').owlCarousel({
	    loop:true,
	    margin:0,
	    nav:true,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:1
	        },
	        1000:{
	            items:1
	        }
	    }
	});

	// Detect Scroll on Mobile

	$('body').on({
	    'touchmove': function(e) { 
	        var scrolltop_cur = $(this).scrollTop();
	        console.log(scrolltop_cur);
	        if(scrolltop_cur >= scrolltop_buffer){
	        	hideMobileMenu();
	        }else{
	        	showMobileMenu();
	        }
	        if(scrolltop_cur <= 20){
	        	showMobileMenu();	
	        }
	        scrolltop_buffer = scrolltop_cur;
	    }
	});

	// Hover Categorias no Menu

	$(".submenu li").mouseenter(function(){
		texto_desc = $(this).children('a').attr('title');
		texto_title = $(this).children('a').text();

		$('.cardapio-tooltip .texto .title').text(texto_title);
		$('.cardapio-tooltip .texto .descricao').text(texto_desc);
	})
});

$(window).scroll(function() {

})

$(window).resize(function() {

})

$(window).load(function() {

});


// Hide Menu
function hideMobileMenu(){
  $('body').addClass('hide-menu');
}

// Show Menu
function showMobileMenu(){
  $('body').removeClass('hide-menu');
}